<?php
namespace ProductList\Classes\Entity;
use ProductList\Classes\DataBase;

class Type implements EntityInterface {

    /**
     * @var int|null
     */
    private $id_type;

    /**
     * @var string
     */
    public string $name_type;

    public static function tableName(): string
    {
        return 'type';
    }

    public function getIdType(): ?int
    {
        return $this->id_type;

    }

    public function setIdType($idType): self
    {
        $this->id_type = $idType;

        return $this;
    }

    public function getNameType(): string
    {
        return $this->name_type;

    }

    public function setNameType(string $nameType): self
    {
        $this->name_type = $nameType;

        return $this;
    }

    public static function getTypes()
    {
        return DataBase::find()
            ->setTableName(self::tableName())
            ->all()
        ;
    }

}
