<?php
namespace ProductList\Classes\Entity\ProductsTypes;

use ProductList\Classes\Entity\Product;

class Book extends Product
{

    public function getProductType(): string
    {
        return 'Book';
    }

    public function getWeight(): int
    {
        return $this->properties['weight'];

    }

    public function setWeight($weight): self
    {
        $this->properties['weight'] = $weight;

        return $this;
    }
}
