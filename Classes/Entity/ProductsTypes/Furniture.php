<?php
namespace ProductList\Classes\Entity\ProductsTypes;

use ProductList\Classes\Entity\Product;

class Furniture extends Product
{
    public function getProductType(): string
    {
        return 'Furniture';
    }

    public function getHeight(): int
    {
        return $this->properties['height'];
    }

    public function setHeight($height): self
    {
        $this->properties['height'] = $height;

        return $this;
    }

    public function getWidth(): int
    {
        return $this->properties['width'];
    }

    public function setWidth($width): self
    {
        $this->properties['width'] = $width;

        return $this;
    }

    public function getLength(): int
    {
        return $this->properties['length'];
    }

    public function setLength($length): self
    {
        $this->properties['length'] = $length;

        return $this;
    }
}
