<?php
namespace ProductList\Classes\Entity\ProductsTypes;

use ProductList\Classes\Entity\Product;

class Dvd extends Product
{
    public function getProductType(): string
    {
        return 'Dvd';
    }

    public function getSize(): int
    {
        return $this->properties['size'];

    }

    public function setSize(int $size): self
    {
        $this->properties['size'] = $size;

        return $this;
    }
}
