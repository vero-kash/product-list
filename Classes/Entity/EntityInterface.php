<?php

namespace ProductList\Classes\Entity;

interface EntityInterface
{
    public static function tableName(): string;
}
