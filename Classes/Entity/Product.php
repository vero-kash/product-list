<?php
namespace ProductList\Classes\Entity;

abstract class Product implements EntityInterface {

    /**
     * @var int|null
     */
    private ?int $id_product;

    /**
     * @var string
     * @feature unique
     */
    public string $sku;

    /**
     * @var string
     */
    public string $name;

    /**
     * @var int|float
     */
    public int|float $price;

    /**
     * @var int
     */
    public int $id_type;

    /**
     * @var array
     * @feature json
     */
    public array $properties = [];

    abstract public function getProductType(): string;

    public static function tableName(): string
    {
        return 'product';
    }

    public function getIdProduct(): ?int
    {
        return $this->id_product;
    }

    public function setIdProduct(int $idProduct): self
    {
        $this->id_product = $idProduct;

        return $this;
    }

    public function getSku(): string
    {
        return $this->sku;
    }

    public function setSku(string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice()
    {
        return number_format($this->price, 2);
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getIdType(): int
    {
        return $this->id_type;
    }

    public function setIdType(int $idType): self
    {
        $this->id_type = $idType;

        return $this;
    }

    public function getProperties(): array
    {
        return $this->properties;
    }

    public function setProperties(array $properties): self
    {
        $this->properties = $properties ;

        return $this;
    }
}
