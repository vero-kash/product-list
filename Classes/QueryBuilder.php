<?php
namespace ProductList\Classes;

class QueryBuilder extends Builder
{
    private $select = '*';
    private $join = '';
    private $where = '';
    private $params = [];
    private $limit = null;

    public function setTableName(string $tableName): self
    {
        $this->tableName = $tableName;

        return $this;
    }

    public function select(string ... $properties): self
    {
        $this->select = implode(', ', $properties);

        return $this;
    }

    public function where(string $propertyName, string $searchValue, $separator = '='): self
    {
        if ($this->where) {
            $this->where .= ' AND ';
        }

        $this->where .= "{$propertyName} {$separator} :{$propertyName}";

        $this->params[$propertyName] = $searchValue;

        return $this;
    }

    public function toSql():string
    {
        return 'SELECT ' . $this->select
            . ' FROM ' . $this->tableName . $this->join
            . ' WHERE ' . (empty($this->where) ? 1 : $this->where)
            . ($this->limit ? ' LIMIT ' . $this->limit : '')
        ;
    }

    public function one(): ?array
    {
        $this->limit = 1;

        return $this->all()[0] ?? [];
    }

    public function all(): ?array
    {
        return $this->db->query($this->toSql(), $this->params) ?? [];
    }

    protected function getListFeatures(): array
    {
        return [];
    }
}
