<?php
namespace ProductList\Classes;

class DataBase{
    private static \PDO $db;

    private static $instances = [];

    protected function __construct() {

    }

    protected function __clone() { }

    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    private function initConnect()
    {

        try {
            self::$db = new \PDO('mysql:host=' . DB_HOST .
                ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET,
                DB_USER, DB_PASS);
        }
        catch(\PDOException $e) {
            echo $e->getMessage();
        }

    }

    public static function getConnect(): DataBase
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static();
            self::$instances[$cls]->initConnect();
        }

        return self::$instances[$cls];
    }

    public function queryInsert($sql, $params = []): ?int
    {
        $statement = self::$db->prepare($sql);

        if ( !empty($params) ) {
            foreach ($params as $key => $value) {
                $statement->bindValue(":$key", $value);
            }
        }

        return $statement->execute() ? self::$db->lastInsertId() : null;
    }

    public function query($sql, $params = [])
    {
        $statement = self::$db->prepare($sql);

        if ( !empty($params) ) {
            foreach ($params as $key => $value) {
                $statement->bindValue(":$key", $value);
            }
        }

        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public static function find(): QueryBuilder
    {
        return new QueryBuilder(static::getConnect());
    }

    public static function insert(): InsertBuilder
    {
        return new InsertBuilder(static::getConnect());
    }

    public static function findOne($nameTable, $selectCol, $whereCol, $value): string
    {
        $array = static::getConnect()->query( "SELECT `{$selectCol}` FROM `{$nameTable}` WHERE `{$whereCol}`='{$value}'");
        return $array[0][$selectCol];
    }


    public static function deleteIn($nameTable, $columnName, $array): bool
    {
        static::getConnect();


        $arr = implode(",",$array);
        $sql = "DELETE FROM {$nameTable} WHERE {$columnName} IN({$arr})";

        $statement = self::$db->prepare($sql);
        return $statement->execute();
    }

}
