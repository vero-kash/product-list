<?php
namespace ProductList\Classes;

use ProductList\Classes\Entity\EntityInterface;
use ProductList\Classes\Entity\Product;
use ProductList\Classes\Exceptions\FieldUniqueException;

class InsertBuilder extends Builder
{
    private $properties = [];

    public function toSql():string
    {
        $properties = "(" . implode(', ', $this->properties) . ")";
        $values = "(:" . implode(', :', $this->properties) . ")";

        return "INSERT INTO " . $this->tableName
            .  " " . $properties
            . " VALUES " . $values
        ;
    }

    public function saveEntity(EntityInterface $entity): ?int
    {
        $this->tableName = $entity::tableName();
        $properties = get_object_vars($entity);
        $ref = new \ReflectionClass($entity::class);

        $params = [];
        foreach ($properties as $pKey => &$pVal) {

            $this->featurePrepare($ref, $pKey, $pVal);

            $this->properties[] = $pKey;
            $params[$pKey] = $pVal;
        }
        unset($pVal);

        return $this->db->queryInsert($this->toSql(), $params);
    }

    protected function getListFeatures(): array
    {
        return ['json', 'unique'];
    }

    /**
     * @param mixed $propertyVal
     *
     * @return string
     */
    protected function jsonFeature(mixed $propertyVal): string
    {
        return json_encode($propertyVal);
    }

    /**
     * @param mixed $propertyVal
     * @param string $propertyName
     *
     * @return string
     * @throws FieldUniqueException
     */
    protected function uniqueFeature(mixed $propertyVal, string $propertyName): void
    {
        $product = DataBase::find()
            ->setTableName(Product::tableName())
            ->where($propertyName, $propertyVal)
            ->one()
        ;

        if ($product) {

            $messageError = sprintf("Field '%s' must be unique!", $propertyName);

            throw new FieldUniqueException($propertyName, $messageError);
        }
    }
}
