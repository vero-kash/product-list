<?php
namespace ProductList\Classes;
class Router
{
    private array $pages = array();

    function addRoute($url, $path){
        $this->pages[$url] = $path;
    }

    function route(){
        $url = $_SERVER['REQUEST_URI'];
        $arrayUrl = explode('?', $url);

        $url = str_replace("_",".",$arrayUrl[0]);


        if (array_key_exists($url, $this->pages)) {
            $path = $this->pages[$url];
            require $path;
        } else {
            require "views/404.php";
            die();
        }
    }
}
