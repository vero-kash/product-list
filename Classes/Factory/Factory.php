<?php
namespace ProductList\Classes\Factory;

class Factory
{
    public static function getFactoryClasses()
    {
        static $classes = [];

        if (!$classes) {
            $classes = include_once __DIR__ . '/factoryClasses.php';
        }

        return $classes;
    }
}
