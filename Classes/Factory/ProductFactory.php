<?php
namespace ProductList\Classes\Factory;

use ProductList\Classes\Entity\Product;
use ProductList\Classes\Repository\TypeRepository;

abstract class ProductFactory extends Factory
{
    public static function createProduct(array $data): Product
    {
        if (!array_key_exists('properties', $data)) {
            throw new \Exception('field properties not exist!');
        }

        if (!is_array($data['properties'])) {
            $data['properties'] = json_decode($data['properties'], true);
        }

        if (!array_key_exists('productType', $data['properties'])) {
            throw new \Exception('field productType not exist!');
        }

        $classes = self::getFactoryClasses();

        if (!array_key_exists($data['properties']['productType'], $classes)) {
            throw new \Exception('Class ' . $data['properties']['productType'] . ' not exist!');
        }

        /** @var Product $productSomeType */
        $productSomeType = new $classes[$data['properties']['productType']];

        if (!array_key_exists('id_type', $data)) {
            $data['id_type'] = TypeRepository::getTypeByName($data['properties']['productType'])->getIdType();
        }

        if(array_key_exists('id_product', $data)){
            $productSomeType->setIdProduct((int) $data['id_product']);
        }

        return $productSomeType
            ->setIdType((int) $data['id_type'])
            ->setSku($data['sku'])
            ->setName($data['name'])
            ->setPrice((float) $data['price'])
            ->setProperties($data['properties'])
        ;
    }


}
