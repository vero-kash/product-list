<?php

return [
    'Book'      => 'ProductList\Classes\Entity\ProductsTypes\Book',
    'DVD'       => 'ProductList\Classes\Entity\ProductsTypes\Dvd',
    'Furniture' => 'ProductList\Classes\Entity\ProductsTypes\Furniture',
];
