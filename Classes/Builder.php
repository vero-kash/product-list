<?php
namespace ProductList\Classes;


abstract class Builder
{
    protected DataBase $db;

    protected string $tableName = 'tableName';

    public function __construct(DataBase $db)
    {
        $this->db = $db;
    }

    abstract protected function getListFeatures(): array;

    protected function featurePrepare(\ReflectionClass $ref, string $propertyName, &$propertyVal): void
    {
        if (!$comments = $ref->getProperty($propertyName)->getDocComment()){
            return;
        }

        preg_match_all("|\@feature\s(.*)\s|", $comments, $featureMatches);

        if (!$featureMatches[1]) {
            return;
        }

        foreach ($featureMatches[1] as $feature) {
            if (is_null(array_search($feature, $this->getListFeatures()))) {
                continue;
            }

            $feature = $feature . 'Feature';

            if (!method_exists($this, $feature)) {
                continue;
            }

            if ($result = $this->{$feature}($propertyVal, $propertyName)) {
                $propertyVal = $result;
            }
        }

    }
}
