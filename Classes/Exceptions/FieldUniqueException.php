<?php

namespace ProductList\Classes\Exceptions;

use Throwable;

class FieldUniqueException extends \Exception
{
    private $fieldName;

    public function __construct(string $fieldName = "", $message = "", $code = 0, Throwable $previous = null)
    {
        $this->fieldName = $fieldName;
        parent::__construct($message, $code, $previous);
    }

    public function getFieldName()
    {
        return $this->fieldName;
    }

}
