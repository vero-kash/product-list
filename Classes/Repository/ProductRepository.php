<?php
namespace ProductList\Classes\Repository;
use ProductList\Classes\DataBase;
use ProductList\Classes\Entity\Product;
use ProductList\Classes\Factory\ProductFactory;

class ProductRepository
{
    public static function getProducts()
    {
        $products = DataBase::find()
            ->setTableName(Product::tableName())
            ->all()
        ;

        $result = [];

        foreach ($products as $product) {

            $result[] = ProductFactory::createProduct($product);
        }
        return $result;
    }

    public static function saveProduct(Product $product): void
    {
        $productId = DataBase::insert()->saveEntity($product);

        if (!$productId) {
            throw new \Exception('product is not save');
        }

        $product->setIdProduct($productId);
    }

}
