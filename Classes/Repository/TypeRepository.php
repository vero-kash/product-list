<?php
namespace ProductList\Classes\Repository;
use ProductList\Classes\DataBase;
use ProductList\Classes\Entity\Product;
use ProductList\Classes\Entity\Type;
use ProductList\Classes\Factory\ProductFactory;

class TypeRepository
{
    public static function getTypes(): array
    {
        static $allProductTypes = [];

        if (!$allProductTypes) {
            $allProductTypes = DataBase::find()
                ->setTableName('type')
                ->all()
            ;

            $allProductTypes = array_column($allProductTypes, null, 'name_type');

            foreach($allProductTypes as &$productType) {
                $productType = (new Type())
                    ->setIdType($productType['id_type'])
                    ->setNameType($productType['name_type'])
                ;
            }

            unset($productType);
        }

        return $allProductTypes;
    }

    public static function getTypeByName(string $typeName): ?Type
    {
        return self::getTypes()[$typeName] ?? null;
    }

}
