<?php

namespace ProductList\Classes;

class ViewTemplate
{
    protected string $file;
    public array $params = [];

    function __construct( $file ) {

        if (!file_exists( $file )) {
            throw new \Exception('unable to locate file');
        }

        $this->file = $file;
    }

    public function setData(array $params): self
    {
        $this->params = $params;

        return $this;
    }

    public function renderHTML(): void
    {
        extract($this->params, EXTR_PREFIX_SAME, "wddx");

        include $this->file;
    }
}
