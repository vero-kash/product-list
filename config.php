<?php
spl_autoload_register(function ($class) {

    $prefix = 'ProductList\\Classes\\';
    $base_dir = __DIR__ . '/Classes/';

    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }

    $relative_class = substr($class, $len);
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

    if (file_exists($file)) {
        require $file;
    }
});

require_once("configDB.php");
require_once("functions.php");
