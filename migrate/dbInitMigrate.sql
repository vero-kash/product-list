--
-- Структура таблицы `type`
--
CREATE TABLE `type` (
    `id_type` int NOT NULL,
    `name_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `type`
--
INSERT INTO `type` (`id_type`, `name_type`) VALUES
(1, 'DVD'),
(2, 'Furniture'),
(3, 'Book');

--
-- Индексы таблицы `type`
--
ALTER TABLE `type`
    ADD PRIMARY KEY (`id_type`);

--
-- AUTO_INCREMENT для таблицы `type`
--
ALTER TABLE `type`
    MODIFY `id_type` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Структура таблицы `product`
--
CREATE TABLE `product` (
   `id_product` int NOT NULL,
   `sku` varchar(14) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
   `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
   `price` decimal(10,2) NOT NULL,
   `id_type` int NOT NULL,
   `properties` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
    ADD PRIMARY KEY (`id_product`),
    ADD KEY `id_type` (`id_type`),
    ADD UNIQUE(`sku`);

--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
    MODIFY `id_product` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- Ограничения внешнего ключа таблицы `product`
--
ALTER TABLE `product`
    ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `type` (`id_type`) ON DELETE RESTRICT ON UPDATE RESTRICT;
