<?php
/**
 * @var array $products
 * @var ProductList\Classes\Entity\Product $product
 */
?>


<?php foreach ($products as $product): ?>
    <div class="col">
        <div class="card h-100">
            <div class="card-body">
                <div class="form-check">
                    <input class="form-check-input delete-checkbox" type="checkbox" value="<?= $product->getIdProduct() ?>" id="">
                </div>
                <h5 class="card-title text-center"><?= $product->getSku() ?></h5>
                <p class="card-text text-center"><?= $product->getName() ?></p>
                <p class="card-text text-center"><?= $product->getPrice() ?> $</p>
                <p class="card-text text-center">
                    <?php view('lists/' . mb_strtolower($product->getProductType()), [
                            'product' => $product,
                    ]); ?>
                </p>
            </div>
        </div>
    </div>
<?php endforeach; ?>
