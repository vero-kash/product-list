<div class="row mb-3">
    <label for="price" class="col-sm-2 col-form-label">Height (CM)</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="height" name="properties[height]" required pattern="^\d{1,4}$" min="1">
    </div>
</div>
<div class="row mb-3">
    <label for="price" class="col-sm-2 col-form-label">Width (CM)</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="width" name="properties[width]" required pattern="^\d{1,4}$" min="1">
    </div>
</div>
<div class="row mb-3">
    <label for="price" class="col-sm-2 col-form-label">Length (CM)</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="length" name="properties[length]" required pattern="^\d{1,4}$" min="1">
    </div>
</div>

Please, provide dimensions in HxWxL format
