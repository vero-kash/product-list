<?php
/**
 * @var array $allTypesProducts\
 * @var \ProductList\Classes\Entity\Type $type
 */
?>

<div class="row mb-3">
    <label for="productType" class="col-sm-2 col-form-label">Type Switcher</label>
    <div class="col-sm-10">
        <select name="properties[productType]" id="productType" class="form-select" aria-label="Default select example" required>

            <option value="">Type Switcher</option>

            <?php foreach ($allTypesProducts as $type): ?>
                <option value="<?= $type->getNameType() ?>">
                    <?= $type->getNameType() ?>
                </option>
            <?php endforeach; ?>

        </select>
    </div>
</div>



