<?php
/**
 * @var array $allTypesProducts
 */
?>

<form id="product_form" action="/create" method="post">
    <div class="row mb-3">
        <label for="sku" class="col-sm-2 col-form-label">SKU</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="sku" name="sku" required pattern="^[A-Za-z|a-zA-z]+[0-9]+" minlength="8" maxlength="14">
        </div>
    </div>
    <div class="row mb-3">
        <label for="name" class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" required pattern="^[A-Za-z|a-zA-z|\s]+([0-9]+)?" minlength="3" maxlength="12">
        </div>
    </div>
    <div class="row mb-3">
        <label for="price" class="col-sm-2 col-form-label">Price ($)</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="price" name="price" required pattern="^([1-9]|[0-9])+(\.[0-9]{2})?" minlength="1" maxlength="11">
        </div>
    </div>

    <?php view('forms/type-select-form', [
        'allTypesProducts' => $allTypesProducts,
    ]); ?>

</form>
