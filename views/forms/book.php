<div class="row mb-3">
    <label for="price" class="col-sm-2 col-form-label">Weight (KG)</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="weight" name="properties[weight]" required pattern="^\d{1,10}$" minlength="1" maxlength="10">
    </div>
</div>

Please, provide weight in KG
