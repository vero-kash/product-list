<header class="add-product-header d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-bottom">
    <a class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none" href="/">
        Product Add
    </a>
    <div id="header-title"></div>
    <div class="col-md-3 text-end" id="button-header">
        <button type="submit" class="btn-outline-dark" form="product_form">Save</button>
        <button type="button" class="btn-outline-dark" onclick="document.location='/'">Cancel</button>
    </div>
</header>


