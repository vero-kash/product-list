<?php

if (!function_exists('view')) {
    function view(string $template, array $params = []): void {
        $template = new ProductList\Classes\ViewTemplate(__DIR__ . '/views/' . $template . '.php');

        if ($params) {
            $template->setData($params);
        }

        $template->renderHTML();
    }
}
