messageInput('#sku');
messageInput('#name');
messageInput('#price');
messageInput('#productType');

function messageInput(elem){
    let input = document.querySelector(elem);

    if (input != null) {
        input.addEventListener('input', (event) => {
            input.setCustomValidity('');
            input.checkValidity();
        });

        input.addEventListener('invalid', (event) => {

            if(input.value === '') {
                input.setCustomValidity('Please, submit required data');
            } else {
                input.setCustomValidity('Please, provide the data of indicated type');
            }

        });

    }
}


let select = document.querySelector('#productType');
let array = [];
select.addEventListener('change', function (){
    if ( select.value != ''){

        // создать див
        let div = document.querySelector('.switcher');

        if(div == null){
            let form = document.querySelector('#product_form');
            div = document.createElement('div');
            div.classList.add('switcher');
            form.appendChild(div);
        }

        let value = select.value;
        let url = '/views/forms/'+ value +'.php';

        fetch(url.toLowerCase()).then(
            response => {
                return response.text();
            }
        ).then(
            text => {
                div.innerHTML = text;
                let inputs = div.querySelectorAll('input');
                inputs.forEach(element => messageInput('#' + element.getAttribute('id')));
                array = [];
            }
        );
    }
})


let form = document.querySelector('#product_form');

form.addEventListener('submit', function(event) {

    fetch('/create', {
        method: 'POST',
        body: new FormData(this),
    }).then(
        response => {

            let elems = document.querySelectorAll('.alert.alert-danger');

            Array.prototype.forEach.call(elems, (node) => {
                node.parentNode.removeChild(node);
            });

            return response.json();
        }
    ).then(
        json => {
            if (json.status === 'ok') {
                document.location='/';
            } else {

                let erDiv = document.createElement('div');
                erDiv.classList.add('alert', 'alert-danger');
                erDiv.innerHTML = json.error.message;
                erDiv.setAttribute('role', 'alert');

                let input = document.querySelector('#' + json.error.field);
                input.parentElement.appendChild(erDiv);
            }
        }
    );

    event.preventDefault();
});
