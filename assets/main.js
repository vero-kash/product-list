    function getAllProducts() {
        let allProduct = document.querySelector('.all_product');

        fetch('/get-products').then(
            response => {
                return response.text();
            }
        ).then(
            text => {
                allProduct.innerHTML = text;
            }
        );
    }

    getAllProducts();

    function getCheckedCheckBoxes() {
        let checkboxes = document.getElementsByClassName('delete-checkbox');
        let checkboxesChecked = [];
        for (let index = 0; index < checkboxes.length; index++) {
            if (checkboxes[index].checked) {
                checkboxesChecked.push(checkboxes[index].value);
            }
        }
        return checkboxesChecked;
    }

    let deleteButton = document.querySelector('#delete-product-btn');
    deleteButton.addEventListener('click', function (){
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if(xhr.readyState === 4) {
                if(xhr.status === 200) {
                    getAllProducts();
            }
        }
    }

    let json = JSON.stringify(getCheckedCheckBoxes());
    xhr.open("POST", '/delete');
    xhr.send(json);
});
