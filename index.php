<?php
require_once("config.php");
use \ProductList\Classes\Router;

$url = key($_GET);

$r = new Router();
$r->addRoute("/","actions/main.php");
$r->addRoute("/add-product","actions/add-product.php");
$r->addRoute("/create","actions/create.php");
$r->addRoute("/delete","actions/delete.php");

$r->addRoute("/get-products","actions/get-products.php");
$r->route();
