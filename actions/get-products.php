<?php
use ProductList\Classes\Repository\ProductRepository;

view('products-list', [
    'products' => ProductRepository::getProducts(),
]);
