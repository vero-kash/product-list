<?php view('layouts/head'); ?>

<body>
<div class="container">
    <?php view('layouts/main-header'); ?>
    <div class="all_product row row-cols-1 row-cols-md-4 g-4"></div>
    <?php view('layouts/footer'); ?>
</div>

<script src="/assets/main.js"></script>

</body>
</html>
