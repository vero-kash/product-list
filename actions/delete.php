<?php
use ProductList\Classes\DataBase;

if (isset($_POST)) {
    $json_str = file_get_contents('php://input');
    $json_obj = json_decode($json_str);

    DataBase::deleteIn('product', 'id_product', $json_obj);
}
