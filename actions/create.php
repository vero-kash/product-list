<?php
use ProductList\Classes\Repository\ProductRepository;
use ProductList\Classes\Factory\ProductFactory;

$post = $_POST;

if (!empty($post['sku']) && !empty($post['name']) && !empty($post['price']) && !empty($post['properties']))
{
    $newProduct = ProductFactory::createProduct($post);

    try {
        ProductRepository::saveProduct($newProduct);
        http_response_code(201);
        echo json_encode(['status' => 'ok']);
    } catch (\ProductList\Classes\Exceptions\FieldUniqueException $e) {
        http_response_code(200);
        echo json_encode([
            'status' => 'error',
            'error' => [
                'field' => $e->getFieldName(),
                'message' => $e->getMessage(),
            ],
        ]);
        exit;
    }
}
