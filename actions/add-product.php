<?php
$allTypesProducts = \ProductList\Classes\Repository\TypeRepository::getTypes();
?>
<?php view('layouts/head'); ?>

<body>

    <div class="container">
        <?php view('layouts/add-product-header'); ?>
        <?php view('forms/product-form', [
            'allTypesProducts' => $allTypesProducts,
        ]); ?>
        <?php view('layouts/footer'); ?>
    </div>

    <script src="/assets/create-product-form.js"></script>
</body>
